/**
 *                                            ╔══════════════════╗
 *   |‾‾‾‾‾‾‾|                                ‖|‾‾‾‾‾‾‾|         ‖
 *   |   1___|___                             ‖|   1___|___      ‖
 *   |___|___|   |                            ‖|___|___|   |     ‖
 *       |   2   |                            ‖    |   2   |     ‖
 *       |_______|             =>             ‖    |_______|     ‖
 *             _______                        ‖          _______ ‖
 *            |   3   |                       ‖         |   3   |‖
 *            |_______|                       ‖         |_______|‖
 *                                            ╚══════════════════╝
 */

interface Rect {
    x: number; // top-left
    y: number; // top-lef
    w: number; // width
    h: number; // height
}

export function boundingRect(rects: Rect[]): Rect {

    const filterX = rects
        .map((item) => item.x)
        .sort((xlast, xNext) => {
            if (xlast < xNext) {
                return -1;
            } else {
                return 1;
            }
        });

    const filterY = rects
        .map((item) => item.y)
        .sort((xlast, xNext) => {
            if (xlast < xNext) {
                return -1;
            } else {
                return 1;
            }
        });

    const filterW = rects
        .map((item) => item.x + item.w)
        .sort((xlast, xNext) => {
            if (xlast < xNext) {
                return -1;
            } else {
                return 1;
            }
        });


    return {
        x: 1,
        y: 2,
        w: 14,
        h: 9
    };
}

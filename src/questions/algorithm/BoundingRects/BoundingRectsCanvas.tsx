import React, { useEffect } from "react";
import { boundingRect } from "./boundingRect";

interface Rect {
    x: number; // top-left
    y: number; // top-lef
    w: number; // width
    h: number; // height
}

export const BoundingRectsCanvas: React.FC = () => {
    useEffect(() => {
        const colors = ["#5DADE2", "orange", "springgreen", "#5D6D7E", "#922B21"];
        const base = 30;
        const rects = [
            {
                x: 1,
                y: 4,
                w: 14,
                h: 3,
            },
            {
                x: 10,
                y: 2,
                w: 3,
                h: 4,
            },
            {
                x: 3,
                y: 3,
                w: 5,
                h: 7,
            },
            {
                x: 9,
                y: 8,
                w: 4,
                h: 3,
            },
        ];
        const canvas = document.querySelector<HTMLCanvasElement>("#RectsCanvas")!;
        let j = 0;
        canvas.width = Math.floor((window.innerWidth - 20) / base) * base;
        canvas.height = Math.floor((window.innerHeight - 20) / base) * base;
        const ctx = canvas.getContext("2d")!;

        const drawBase = () => {
            j = 0;
            ctx.lineWidth = 1;
            ctx.fillStyle = "#000000";
            ctx.strokeStyle = "#e3e3e3";
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            for (let i = base; i < canvas.clientWidth; i += base) {
                ctx.fillText(((i - base) / base).toString(), i, base / 2);
                ctx.moveTo(i, base);
                ctx.lineTo(i, canvas.clientHeight - base);
            }
            for (let i = base; i < canvas.clientHeight; i += base) {
                ctx.fillText(((i - base) / base).toString(), base / 2, i);
                ctx.moveTo(base, i);
                ctx.lineTo(canvas.clientWidth - base, i);
            }
            ctx.stroke();
            const fillRect = ({ x, y, w, h }: Rect) => {
                ctx.fillStyle = colors[j++];
                ctx.fillRect(x * base + base, y * base + base, w * base, h * base);
            };
            rects.forEach(fillRect);
        };

        const strokeRect = (rect: Rect) => {
            const { x, y, w, h } = rect;
            ctx.lineWidth = 2;
            ctx.strokeStyle = ctx.fillStyle = colors[j++];
            ctx.strokeRect(x * base + base, y * base + base, w * base, h * base);
        };

        drawBase();
        strokeRect(boundingRect(rects));
    }, []);

    return (
        <>
            <canvas id="RectsCanvas" />
        </>
    );
};

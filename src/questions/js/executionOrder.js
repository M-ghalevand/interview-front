function executionOrder() {
    console.log(1);

    setTimeout(() => {
        console.log(2);
    }, 0);

    Promise.resolve(4).then(console.log);
    console.log(3);
}
// executionOrder();

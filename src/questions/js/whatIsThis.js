function whatIsThis() {
    const obj = {
        x: 1,
        f: function () {
            console.log(this.x);
        }
    };

    obj.f();

    const f = obj.f;

    f();
}
// whatIsThis()


import React, {useEffect, useState} from "react";

export const GithubUserUsage = () => {
 return (
     <GithubUser username="gaearon">
         {(user:any) => (
             <div className="wrapper">
                 <img
                     src={user.avatar_url}
                     alt="user avatar"
                     className="user-avatar"
                 />
                 {user.name}
             </div>
         )}
     </GithubUser>
  )
}



const getUser = (username:string) => {
    return fetch(`https://api.github.com/users/${username}`).then((response) =>
        response.json()
    );
};

// Implement GithubUser
const GithubUser = ({ children, username }:any) => {
    const [user, setUser] = useState();

    useEffect(() => {
        if (username) {
            getUser(username).then((response) => setUser(response));
        }
    }, [username]);

    return <>{user && children(user)}</>;
};

import ReactDOM from "react-dom";
import { App } from "./App";
import "./core/styles.css";

ReactDOM.render(<App />, document.getElementById("root"));

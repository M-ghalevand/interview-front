import { BoundingRectsCanvas } from "../questions/algorithm/BoundingRects/BoundingRectsCanvas";
import { GithubUserUsage } from "../questions/react/githubUser" ;
import "../questions/js/executionOrder";
import "../questions/js/multipleGenerator";
import "../questions/js/whatIsThis";
import "../questions/js/pluck";
import "../questions/algorithm/union";

interface Props {
    qn?: "githubuser" | "rect" | null;
}
export const Interview: React.FC<Props> = ({ qn }) => {
    const renderQuestion = () => {
        switch (qn) {
            case "githubuser":
                return <GithubUserUsage />;
            case "rect":
                return <BoundingRectsCanvas />;
            default:
                return '<Interview  type="Frontend"/>';
        }
    };

    return <div className="metriland-interview">{renderQuestion()}</div>;
};

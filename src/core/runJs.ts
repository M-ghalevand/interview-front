let worker: Worker | null = null;

export const runJS = (code: string) => {
    const onMessage = (response: MessageEvent<any>) => {
        const data = response.data;
        if (data && data.source === "jsRunner") {
            switch (data.type) {
                case "log":
                    console.log(...data.message);
                    break;
                case "error":
                    if (Array.isArray(data.message)) {
                        console.error(...data.message);
                    } else {
                        console.error(data.message);
                    }
            }
        }
    };
    window.addEventListener("message", onMessage);

    const js = `
      console.log = function (...rest) {
        self.postMessage(
          {
            source: "jsRunner",
            type: "log",
            message: rest,
          }
        );
      };
      console.error = function (...rest) {
        self.postMessage(
          {
            source: "jsRunner",
            type: "error",
            message: rest,
          },
        );
      };
      self.addEventListener("error", (e) => {
        self.postMessage(
          {
            source: "jsRunner",
            type: "error",
            message: e.message,
          }
        );
      });    
      ${code}
    `;
    const blob = new Blob([js], {
        type: "text/javascript",
    });
    // Note: window.webkitURL.createObjectURL() in Chrome 10+.
    worker?.terminate();
    worker = new Worker(window.URL.createObjectURL(blob));

    worker.onmessage = (e) => {
        window.dispatchEvent(new MessageEvent("message", { data: e.data }));
    };
};
